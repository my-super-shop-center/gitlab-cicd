package com.example.gitlabcicd.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ArticleControllerTest extends AbstractIntegrationTest {

    @Test
    public void shouldSaveArticle_thenStatusIsSuccessful() {

    }

    @Test
    @Override
    public void saveEntity_saveSuccessfulStatusIsOk() throws Exception {
        this.mockMvc
                .perform(post("/article/save-article")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "   \"title\":\"title5\",\n" +
                                "   \"text\":\"text5\"\n" +
                                "}"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @Override
    public void getEntityById_returnOnlyEntityStatusIsOk() throws Exception {
        this.mockMvc
                .perform(get("/article/{id}", "10"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is("title1")))
                .andReturn().getResponse().getContentAsString();
    }
}
