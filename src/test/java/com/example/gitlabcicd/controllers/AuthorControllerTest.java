package com.example.gitlabcicd.controllers;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.iterableWithSize;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.gitlabcicd.dto.AuthorDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class AuthorControllerTest extends AbstractIntegrationTest {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Test
    public void getAuthors_returnsAllElements() throws Exception {
        this.mockMvc
                .perform(get("/author/authors"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.size()", greaterThanOrEqualTo(3)));
    }

    @Test
    @Override
    public void getEntityById_returnOnlyEntityStatusIsOk() throws Exception {
        this.mockMvc
                .perform(get("/author/get-author/{name}", "bob1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("bob1")))
                .andReturn().getResponse().getContentAsString();
    }

    @Test
    @Override
    public void saveEntity_saveSuccessfulStatusIsOk() throws Exception {
        AuthorDto savedAuthor = saveAuthor();
        assertThat(savedAuthor.getId()).isNotNull();
        assertThat(savedAuthor.getTitles().size()).isLessThan(1);
    }

    private AuthorDto saveAuthor() throws Exception {
        AuthorDto author = new AuthorDto();
        author.setName("Igor");

        String postValue = OBJECT_MAPPER.writeValueAsString(author);

        MvcResult mvcResult = this.mockMvc
                .perform(MockMvcRequestBuilders
                        .post("/author/save-author")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(postValue))
                .andExpect(status().is2xxSuccessful())
                .andDo(print())
                .andReturn();

        return OBJECT_MAPPER.readValue(mvcResult.getResponse().getContentAsString(), AuthorDto.class);
    }
}
