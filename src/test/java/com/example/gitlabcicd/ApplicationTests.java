package com.example.gitlabcicd;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@TestPropertySource("/application_test.yml")
class ApplicationTests {

	@Test
	void contextLoads() {
	}

}
