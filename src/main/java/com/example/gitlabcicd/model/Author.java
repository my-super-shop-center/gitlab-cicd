package com.example.gitlabcicd.model;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
@Table(name = "author")
public class Author {
    @Id
    @Column(name = "author_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @JoinTable(name = "author_article",
            joinColumns = @JoinColumn(
                    name = "author_id",
                    referencedColumnName = "author_id"),
            inverseJoinColumns = @JoinColumn(
                    name = "article_id",
                    referencedColumnName = "article_id"))
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Article> articles = new ArrayList<>();
}
