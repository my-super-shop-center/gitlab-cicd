package com.example.gitlabcicd.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class AuthorDto {
    private Long id;
    private String name;
    private List<String> titles = new ArrayList<>();
}
