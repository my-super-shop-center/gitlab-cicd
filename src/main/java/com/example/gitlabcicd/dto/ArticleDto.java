package com.example.gitlabcicd.dto;

import lombok.Data;

@Data
public class ArticleDto {
    private Long id;
    private String title;
    private String text;
}
