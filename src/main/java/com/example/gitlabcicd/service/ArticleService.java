package com.example.gitlabcicd.service;

import com.example.gitlabcicd.dto.ArticleDto;
import com.example.gitlabcicd.mapper.ArticleMapper;
import com.example.gitlabcicd.model.Article;
import com.example.gitlabcicd.repository.ArticleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ArticleService {

    private final ArticleRepository repository;

    public List<ArticleDto> getAllArticles() {
        List<Article> articles = repository.findAll();
        return ArticleMapper.INSTANCE.articleListToArticleDtoList(articles);
    }

    public ArticleDto getArticleById(Long id) {
        Article article = repository.findById(id)
                .orElseThrow(() -> new RuntimeException(String.format("not found article by id %d", id)));
        return ArticleMapper.INSTANCE.articleToArticleDto(article);
    }

    public ArticleDto saveArticle(ArticleDto articleDto) {
        Article article = ArticleMapper.INSTANCE.articleDtoToArticle(articleDto);
        repository.save(article);
        return  ArticleMapper.INSTANCE.articleToArticleDto(article);
    }
}
