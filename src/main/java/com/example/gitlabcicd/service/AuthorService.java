package com.example.gitlabcicd.service;

import com.example.gitlabcicd.dto.AuthorDto;
import com.example.gitlabcicd.mapper.AuthorMapper;
import com.example.gitlabcicd.model.Author;
import com.example.gitlabcicd.repository.AuthorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository repository;

    public List<AuthorDto> findAll() {
        List<Author> authors = repository.findAll();
        return AuthorMapper.INSTANCE.entitiesToDtos(authors);
    }

    public AuthorDto findAuthorByName(String name) {
        Author author = repository.findByName(name).orElse(null);
        return AuthorMapper.INSTANCE.entityToDto(author);
    }

    public AuthorDto saveAuthor(AuthorDto authorDto) {
        Author author = AuthorMapper.INSTANCE.dtoToEntity(authorDto);
        repository.save(author);
        return AuthorMapper.INSTANCE.entityToDto(author);
    }
}
