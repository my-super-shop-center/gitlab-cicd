package com.example.gitlabcicd.mapper;

import com.example.gitlabcicd.dto.ArticleDto;
import com.example.gitlabcicd.model.Article;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ArticleMapper {

    ArticleMapper INSTANCE = Mappers.getMapper(ArticleMapper.class);

    ArticleDto articleToArticleDto(Article article);
    Article articleDtoToArticle(ArticleDto articleDto);
    List<ArticleDto> articleListToArticleDtoList(List<Article> articleList);
    List<Article> articleDtoListToArticleList(List<ArticleDto> articleDtoList);
}
