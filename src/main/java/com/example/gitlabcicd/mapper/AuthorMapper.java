package com.example.gitlabcicd.mapper;

import com.example.gitlabcicd.dto.AuthorDto;
import com.example.gitlabcicd.model.Article;
import com.example.gitlabcicd.model.Author;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Objects;

@Mapper
public interface AuthorMapper {

    AuthorMapper INSTANCE = Mappers.getMapper(AuthorMapper.class);

    @Mapping(source = "articles", target = "titles")
    AuthorDto entityToDto(Author author);

    @Mapping(source = "titles", target = "articles")
    Author dtoToEntity(AuthorDto authorDto);

    @Mapping(source = "articles", target = "titles")
    List<AuthorDto> entitiesToDtos(List<Author> authors);

    @Mapping(source = "titles", target = "articles")
    List<Author> dtosToEntities(List<AuthorDto> authorDtos);

    default String fromArticles(Article article) {
        return Objects.isNull(article) ? "" : article.getTitle();
    }

    default Article fromString(String title) {
        Article article = new Article();
        article.setTitle(title);
        article.setText("new page");
        return article;
    }
}
