package com.example.gitlabcicd.repository;

import java.sql.*;

public class AccountRepository {

    private static final String CONNECTION_URL = "";

    public void useDb() {
        try (Connection connection = DriverManager.getConnection(CONNECTION_URL)){
            Statement statement = connection.createStatement();
            create(statement);
            insert(statement);
            insert(connection);
            select(statement);
            drop(statement);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void create(Statement statement) throws SQLException {
        int i = statement.executeUpdate("create table if not exists account(id int, name text, bic int)");
        System.out.println("created: " + i);

    }

    private void insert(Statement statement) throws SQLException {
        String query = "insert into account (name, bic)values('name', 111), ('name2', 222)";
        int i = statement.executeUpdate(query);
        System.out.println("updated: " + i);
    }

    private void insert(Connection connection) throws SQLException {
        String sql = "insert into account (name, bic) Values (?, ?)";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1, "psq");
        preparedStatement.setInt(2, 123);
        int i = preparedStatement.executeUpdate();

        System.out.println("updated: " + i);
    }

    private void select(Statement statement) throws SQLException {
        String query = "select * from account";
        ResultSet resultSet = statement.executeQuery(query);
        printResult(resultSet);
    }

    private void printResult(ResultSet resultSet) throws SQLException {
        while (resultSet.next()) {
            String name = resultSet.getString("name");
            int bic = resultSet.getInt("bic");
            System.out.println("name: " + name + " bic: " + bic);
        }
    }

    private void drop(Statement statement) throws SQLException {
        int i = statement.executeUpdate("drop table if exists account");
        System.out.println("dropped: " + i);
    }
}
