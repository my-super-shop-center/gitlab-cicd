package com.example.gitlabcicd.repository;

import com.example.gitlabcicd.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ArticleRepository extends JpaRepository<Article, Long> {
}
