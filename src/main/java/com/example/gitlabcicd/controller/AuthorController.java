package com.example.gitlabcicd.controller;

import com.example.gitlabcicd.dto.AuthorDto;
import com.example.gitlabcicd.service.AuthorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/author")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService service;

    @GetMapping(value = "/get-author/{name}")
    public AuthorDto getAuthorByName(@PathVariable String name) {
        return service.findAuthorByName(name);
    }

    @GetMapping(value = "/authors")
    public List<AuthorDto> getAuthors() {
        return service.findAll();
    }

    @PostMapping(value = "/save-author")
    public AuthorDto saveAuthor(@RequestBody AuthorDto authorDto) {
        return service.saveAuthor(authorDto);
    }
}
