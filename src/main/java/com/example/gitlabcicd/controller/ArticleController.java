package com.example.gitlabcicd.controller;

import com.example.gitlabcicd.dto.ArticleDto;
import com.example.gitlabcicd.service.ArticleService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/article")
@RequiredArgsConstructor
public class ArticleController {

    private final ArticleService service;

    @GetMapping(value = "/articles")
    public List<ArticleDto> getArticles() {
        return service.getAllArticles();
    }

    @GetMapping(value = "/{id}")
    public ArticleDto getArticleById(@PathVariable Long id) {
        return service.getArticleById(id);
    }

    @PostMapping(value = "/save-article")
    public ArticleDto saveArticle(@RequestBody ArticleDto articleDto) {
        return service.saveArticle(articleDto);
    }
}
