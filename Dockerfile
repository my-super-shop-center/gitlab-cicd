# the first stage of our build will use amaven 3.8 parent image
FROM maven:3.8-openjdk-11 AS MAVEN_BUILD

# copy the pom and src code to thecontainer
COPY ./ ./

# package applicaction code
RUN mvn clean package

# the second stage of our build will use openjdk 11
FROM openjdk:11.0.7-jdk-slim

# copy only the artifacts weneed from the first stage and discard the rest
COPY --from=MAVEN_BUILD /target/gitlab-cicd-1.0.jar /gitlab-cicd.jar

EXPOSE 3000

# set the startup command to execute the jar
CMD ["java", "-jar", "/gitlab-cicd.jar"]

# docker build -t gitlab-cicd:1.0 .
# docker run -d -p 3000:3000 gitlab-cicd:1.0